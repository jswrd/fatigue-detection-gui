import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http'
import { AppSettings } from './app-const'

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private loggedInStatus = JSON.parse(localStorage.getItem('loggedIn') || 'false')

  constructor(private http: HttpClient) { }

  //function to fetch admins login status
  setLoggedIn(value: boolean){
    this.loggedInStatus = value
    localStorage.setItem('loggedIn', 'true')
  }

  get isLoggedIn(){
    return JSON.parse(localStorage.getItem('loggedIn') || this.loggedInStatus.toString())
  }

  //admin login
  logIn(user_name: any, _password: any){

    return this.http.post(`${AppSettings.API_ENDPOINT}/user/login`, {
      user_name,
      _password
    })
  }
}
