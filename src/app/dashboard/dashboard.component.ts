import { StatsService } from './../stats.service';
import { EmployeeService } from './../employee.service';

import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { Router } from '@angular/router'


// export interface PeriodicElement {
//   name: string;
//   position: number;
//   shiftTime: string;
  
// }






@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

   ELEMENT_DATA: any = [];

   
    
   constructor(private router: Router, private employees: EmployeeService, private stat: StatsService) { }


   
  

  ngOnInit() {

    
    
    


    


    
    //use fetch employee details service

    this.employees.getEmployees().subscribe(data => {

      
      
      

      this.dataSource = new MatTableDataSource(data[0]);

      
      
      
    })
  }

  
  
  

//constructor for employee list table
  displayedColumns: string[] = ['name', 'shiftTime', 'button'];
  dataSource = new MatTableDataSource(this.ELEMENT_DATA);

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  showStats(employee_name){
      // console.log(name);
      localStorage.setItem('employee_name', employee_name)
      this.router.navigate(['stats']);
      
    
      
  }

}


