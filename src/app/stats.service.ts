import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http'
import * as io from 'socket.io-client'
import { AppSettings } from './app-const'

@Injectable({
  providedIn: 'root'
})
export class StatsService {
  socket:any
  constructor(private http: HttpClient) {
    this.socket = io(`${AppSettings.API_ENDPOINT}`)
   }


//open socket connection to get the stats
  listen(Eventname: string){
    return new Observable((subscriber) => {
      this.socket.on(Eventname, (data) => {
        subscriber.next(data);
      })
    })
  } 


  // getStats(name){

  //   return this.http.get(`http://localhost:3000/employee/${name}`)
  // }

  // filterByDate(date: any, name: any){

  //   return this.http.post(`${AppSettings.API_ENDPOINT}/fScore/filterByDate`, {
  //     date,
  //     name
  //   })
  // }


}


