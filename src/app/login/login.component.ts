import { LoginService } from './../login.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private Login: LoginService, private Router:Router) { }

  ngOnInit() {
  }

  //function to login an admin
  logValues(loginForm: NgForm): void {
    const user_name = loginForm.value.username;
    const _password = loginForm.value.password;

    // console.log(username, password);
    
    
    this.Login.logIn(user_name, _password).subscribe(data => {
    
     this.Router.navigate(['/dashboard'])
     this.Login.setLoggedIn(true)
    },
    error => {
      window.alert('Invalid Credentials. Please try again.')
        setTimeout(() => {
          window.location.reload();
        }, 500)
    }
      
      
    )
    
    
  }

}
