import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http'
import { AppSettings } from './app-const'

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) { }



//http request to fetch employee list
  getEmployees(){

    return this.http.get(`${AppSettings.API_ENDPOINT}/employee`)
  }

}
