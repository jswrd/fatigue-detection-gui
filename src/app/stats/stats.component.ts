import { AppSettings } from './../app-const';
import { StatsService } from './../stats.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Chart } from 'chart.js';
import { HttpClient} from '@angular/common/http'





@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css']
})
export class StatsComponent implements OnInit, OnDestroy {

  employee_name;
  barChart;
  pieChart;
  lineChart;
  image;
  url = AppSettings.API_ENDPOINT;
  date='';
  highestScore;
  score;
  constructor(private stats: StatsService, private http: HttpClient) { }

 
  filterByDate(date, name){
    //fetch the fatigue score values filtering by date and employee name
    this.http.post(`${AppSettings.API_ENDPOINT}/fScore/filterByDate`, {date,name}).subscribe((data)=>{
      
      //modify the chart using the response data
      this.barChart.data.datasets[0].data = data;
      this.barChart.data.labels = data;
      this.barChart.update();
      this.stats.socket.emit('end', 'stop')
    })
   
  }

  //fetch the highest score of an employee on a particular date
  getHighestScore(date, name){
    this.http.post(`${AppSettings.API_ENDPOINT}/fScore/bestInDate`, {date,name}).subscribe((data)=>{
      
      this.score = data[0]['MAX(fatigue_score_value)'];
      
    })
   
  }


  reload(){
    window.location.reload()
  }

  



  ngOnInit() {
    // console.log(localStorage.getItem('name'));
    this.employee_name = localStorage.getItem('employee_name')

    // this.image = 'test.jpg'
    
    
  

   this.stats.socket.emit('name', this.employee_name)


    // this.stats.listen('lineChart').subscribe(res => {
    //   // console.log(res);
    //   this.lineChart.data.datasets[0].data = res;
    //   this.lineChart.update();
    // })

    //Fetch the image of an employee generated on a specific time interval
    this.stats.listen('image').subscribe(res => {
      // console.log(res);
      this.image = res
    })

    //modify the barchart using the fetched data
    this.stats.listen('barChart').subscribe(res => {
      // console.log(res);
      this.barChart.data.datasets[0].data = res;
      this.barChart.data.labels = res;
      this.barChart.update();
    })

    



    // this.stats.getStats(this.employee_name).subscribe(data => {
    //   console.log(data);
      
    // })




//init the bar chart
    this.barChart = new Chart('canvas', {
      type: 'bar',
      data: {
        labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        datasets: [{
            type: 'bar',
            label: 'Fatigue Score',
            barThickness: 23,
            backgroundColor: '#d45079',
            data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            fill: false
        }]
    },
      options: {
        title: {
          display: 'false'
        },
        scales: {
          xAxes: [{
              gridLines: {
                  color: "rgba(0, 0, 0, 0)",
              }
          }],
          yAxes: [{
              gridLines: {
                  color: "rgba(0, 0, 0, 0)",
              }   
          }]
      }
      }
  });

//   this.pieChart = new Chart('canvas1', {
//     type: 'pie',
//     data: {
//       labels: ['Completed Duty hours', 'Remaining Duty hours'],
//       datasets: [{
//           type: 'pie',
//           backgroundColor: ['skyblue', 'gray'],
//           data: [20, 10],
//           fill: false
//       }]
//   },
//     options: {
//       title: {
//         display: 'false'
//       }
//     }
// });

// this.lineChart = new Chart('canvas2', {
//   type: 'line',
//   data: {
//     labels: ['10:00','10:00','10:00','10:00','10:00',],
//     datasets: [{
//         type: 'line',
//         label: 'Live Fatigue Score',
//         backgroundColor: 'red',
      
//         // data: [50, 70, 10, 40, 20],
//         fill: false
//     }]
// },
//   options: {
//     title: {
//       display: 'true'
//     }
//   }
// });
    



  
  }


//close the socket temporarily
  ngOnDestroy() {
    this.stats.socket.emit('end', 'stop')
    window.location.reload()
  }


  

}
